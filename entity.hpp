#pragma once

#include <iostream>
#include <cstring>

#ifndef ENTITY_H
#define ENTITY_H

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/quaternion.hpp>

//#include <logger.hpp>

static uint8_t fbInstanceCounter = 0;

typedef uint8_t EntityID;

struct FbGameObject
{
	glm::mat4 	modelMat;
	glm::quat 	orientation;
	glm::vec3 	worldPosition;
	glm::vec3 	scale;

	uint64_t 	objectId;
	uint64_t 	objectFlags : 56;
	uint64_t 	maxComponents : 4;
	uint64_t	numComponents : 4;

	uint8_t*	compType;
	void**		components;

	// 	uint64_t 	objectFlags 	: 56;
#define FLAG_DISPLAY_DEBUG_POINTS 	0x0000000000000001
#define FLAG_OBJECT_WAS_UPDATED		0x0000000000000002

};

enum
{
	MX_RIDIG_BODY
};

struct FbInstance
{
	FbGameObject*	gameObjects;
	uint8_t*		objectDeltas;
	uint32_t 		numLoadedGameObjects;
	uint32_t 		numGameObjects;
	uint64_t		instanceFlags;


	//	uint64_t		instanceFlags;
#define USER_SPECIFIED_FLAGS	0xFFFFFFFF
#define FB_FLAGS_DEBUG_MODE		0x00000001
#define INSTANCE_SET_FLAGS		0xFFFFFFFF00000000
};

FbInstance* createFabrykInstance(const char* filePath, uint32_t instanceFlags, uint32_t maxObjects);
int 		updateObjectModelMatricies(FbInstance* fbInst);

uint32_t 	_debug_setObjectData(FbInstance* fb, glm::vec3 wPosition, uint64_t objId);
uint64_t  	_fbRender_loadObjectNewObj(FbInstance* fb, char* texturePath);

static inline void updateModelMatrix(FbGameObject* temp)
{
	temp->modelMat = glm::translate(glm::mat4(1.0f), temp->worldPosition);
	temp->modelMat *= glm::toMat4(temp->orientation);
	temp->modelMat = glm::scale(temp->modelMat, temp->scale);
}

#endif
