#include "collision.hpp"
#include <assert.h>
#include <math.h>
#include <stdlib.h>
#include <time.h>
#include <cstdlib>
#include <stdio.h>
#include "entity.hpp"
#include "list"

#include "physics_types.h"
#include "collision_queue.h"

#define gkEpsilon FLT_EPSILON

#define eps_rel22        (float) gkEpsilon * 1e4f
#define eps_tot22        (float) gkEpsilon * 1e2f

#define norm2(a)         (a.x*a.x + a.y*a.y + a.z*a.z)

#define S3Dregion1234() \
	*v = glm::vec3(0,0,0); \
	*simplexSize = 4;

#define select_1ik() \
	*simplexSize = 3; \
	simplex[2] = simplex[3]; \
	simplex[1] = si; \
	simplex[0] = sk; 

#define select_1ij() \
	*simplexSize = 3; \
	simplex[2] = simplex[3]; \
	simplex[1] = si; \
	simplex[0] = sj; 

#define select_1jk() \
	*simplexSize = 3; \
	simplex[2] = simplex[3]; \
	simplex[1] = sj; \
	simplex[0] = sk; 

#define select_1i() \
	*simplexSize = 2; \
	simplex[1] = simplex[3]; \
	simplex[0] = si; 

#define select_1j() \
	*simplexSize = 2; \
	simplex[1] = simplex[3]; \
	simplex[0] = sj;

#define select_1k() \
	*simplexSize = 2; \
	simplex[1] = simplex[3]; \
	simplex[0] = sk;

#define calculateEdgeVector(p1p2, p2)                                                                                  \
	p1p2[0] = p2[0] - s->vrtx[3][0];                                                                                     \
	p1p2[1] = p2[1] - s->vrtx[3][1];                                                                                     \
	p1p2[2] = p2[2] - s->vrtx[3][2];

#define S1Dregion1() \
	*simplexSize = 1; \
	*v = simplex[1]; \
	simplex[0] = simplex[1];

#define S2Dregion1() \
	*simplexSize = 1; \
	*v = simplex[2]; \
	simplex[0] = simplex[2];

#define S2Dregion12() \
	*simplexSize = 2; \
	simplex[0] = simplex[2]

#define S2Dregion13() \
	*simplexSize = 2; \
	simplex[1] = simplex[2];

#define S3Dregion1() \
	*simplexSize = 1; \
	*v = s1; \
	simplex[0] = s1;


static void mostSeperatePointsOnAABB(int& min, int& max, glm::vec3* verticies, uint16_t numVert)
{
	int minx = 0, maxx = 0, miny = 0, maxy = 0, minz = 0, maxz = 0;
	for (int i = 0; i < numVert; ++i)
	{
		if (verticies[i].x < verticies[minx].x)
		{
			minx = i;
		}
		if (verticies[i].x > verticies[maxx].x)
		{
			maxx = i;
		}
		if (verticies[i].y < verticies[miny].y)
		{
			miny = i;
		}
		if (verticies[i].y > verticies[maxy].y)
		{
			maxy = i;
		}
		if (verticies[i].z < verticies[minz].z)
		{
			minz = i;
		}
		if (verticies[i].z > verticies[maxz].z)
		{
			maxz = i;
		}
	}

	float dist2x = glm::dot(verticies[maxx] - verticies[minx], verticies[maxx] - verticies[minx]);
	float dist2y = glm::dot(verticies[maxy] - verticies[miny], verticies[maxy] - verticies[miny]);
	float dist2z = glm::dot(verticies[maxz] - verticies[minz], verticies[maxz] - verticies[minz]);

	min = minx;
	max = maxx;

	if (dist2y > dist2x && dist2y > dist2z)
	{
		max = maxy;
		min = miny;
	}
	if (dist2z > dist2x && dist2z < dist2y)
	{
		max = maxz;
		min = minz;
	}

	return;
}

static void sphereFromDistantPoints(MxSphere* objPtr, glm::vec3* verts, uint16_t vertCount)
{
	int min, max;
	//mxInst->pirimitives[objId]
	mostSeperatePointsOnAABB(min, max, verts, vertCount);

	objPtr->centroid = (verts[min] + verts[max]) * 0.5f;
	objPtr->r = glm::dot(verts[max] - objPtr->centroid, verts[max] - objPtr->centroid);
	objPtr->r = sqrt(objPtr->r);
	return;
}

static void sphereOfSphereAndPoints(MxSphere* objPtr, glm::vec3* verts, uint16_t vertCount, uint16_t index)
{
	glm::vec3 d = verts[index] - objPtr->centroid;
	float dist2 = glm::dot(d, d);
	
	if (dist2 > objPtr->r * objPtr->r)
	{
		float dist = sqrt(dist2);
		float newRadius = (objPtr->r + dist) * 0.5f;
		float k = (newRadius - objPtr->r) / dist;
		objPtr->r = newRadius;
		objPtr->centroid += d * k;
	}
	
	return;
}

inline static float determinant(const glm::vec3 p, const glm::vec3 q, const glm::vec3 r)
{
	return p.x * ((q.y * r.x) - (r.y * q.z)) -
		p.y * (q.x * r.z - r.x * q.z) +
		p.z * (q.x * r.y - r.x * q.z);
}

inline static void projectOnLine(const glm::vec3 p,
	const glm::vec3 q,
	glm::vec3* v)
{
	glm::vec3 pq;
	pq = p - q;

	const float tmp = glm::dot(p, pq) / glm::dot(pq, pq);

	*v = p - pq * tmp;
}

inline static void projectOnPlane(const glm::vec3 p,
	const glm::vec3 q,
	const glm::vec3 r,
	glm::vec3* v)
{
	glm::vec3 n, pq, pr;

	pq = p - q;
	pr = p - r;

	n = glm::cross(pq, pr);
	const float tmp = glm::dot(n, p) / glm::dot(n, n);

	*v = n * tmp;
	return;
}

inline static int hff1(const glm::vec3 p,
	const glm::vec3 q)
{
	float tmp = 0;

	tmp += (p.x * p.x - p.x * q.x);
	tmp += (p.y * p.y - p.y * q.y);
	tmp += (p.z * p.z - p.z * q.z);

	if (tmp > 0)
	{
		// keep q
		return 1;
	}
	return 0;
}

inline static int hff2(const glm::vec3 p,
	const glm::vec3 q,
	const glm::vec3 r)
{
	glm::vec3 ntmp;
	glm::vec3 n, pq, pr;

	pq = q - p;
	pr = r - p;

	ntmp = glm::cross(pq, pr);
	n = glm::cross(pq, ntmp);

	return glm::dot(p, n) < 0; // Discard r if true
}

inline static int hff3(const glm::vec3 p,
	const glm::vec3 q,
	const glm::vec3 r)
{
	glm::vec3 n, pq, pr;

	pq = q - p;
	pr = r - p;

	n = glm::cross(pq, pr);
	return glm::dot(p, n) <= 0; // discard s if true
}

static inline uint16_t support(glm::vec3* verticies, uint16_t vert_count, glm::vec3 search_vec)
{
	int index = 0;
	float d_max = glm::dot(verticies[0], search_vec);
	for (int i = 1; i < vert_count; ++i)
	{
		float val = glm::dot(verticies[i], search_vec);
		if (val > d_max)
		{
			index = i;
			d_max = val;
		}
	}
	return index;
}

inline static void S1D(glm::vec3* simplex, uint8_t* simplexSize, glm::vec3* v)
{
	if (hff1(simplex[1], simplex[0]))
	{
		projectOnLine(simplex[1], simplex[0], v); // Update v, no need to update s
		//return;                     // Return V{1,2}
	}
	else
	{
		S1Dregion1(); // Update v and s
		//return;       // Return V{1}
	}
}

inline static void S2D(glm::vec3* simplex, uint8_t* simplexSize, glm::vec3* v)
{
	const int hff1f_s12 = hff1(simplex[2], simplex[1]);
	const int hff1f_s13 = hff1(simplex[2], simplex[0]);

	if (hff1f_s12)
	{
		const int hff2f_23 = !hff2(simplex[2], simplex[1], simplex[0]);
		if (hff2f_23)
		{
			if (hff1f_s13)
			{
				const int hff2f_32 = !hff2(simplex[2], simplex[0], simplex[1]);
				if (hff2f_32)
				{
					// Update s, no need to update c
					// Return V{1,2,3}
					projectOnPlane(simplex[2], simplex[1], simplex[0], v);
					return;
				}
				else
				{
					// Update s && v, Return V{1,3}
					projectOnLine(simplex[2], simplex[0], v);
					S2Dregion13();
					return;
				}
			}
			else
			{
				// Update s, no need to update c
				// Return V{1,2,3}
				projectOnPlane(simplex[2], simplex[1], simplex[0], v);
				return;
			}
		}
		else
		{
			// Update v && s, return V{1, 2}
			projectOnLine(simplex[2], simplex[1], v);
			S2Dregion12();
			return;
		}
	}
	else if (hff1f_s13)
	{
		const int hff2f_32 = !hff2(simplex[2], simplex[0], simplex[1]);
		if (hff2f_32)
		{
			// Update s, no need to update v
			// Return V{1,2,3}
			projectOnPlane(simplex[2], simplex[1], simplex[0], v);
			return;
		}
		else
		{
			// Update v && s, return V{1, 3}
			projectOnLine(simplex[2], simplex[0], v);
			S2Dregion13();
			return;
		}
	}
	else
	{
		// Update s and v
		// Return V{1}
		S2Dregion1();
		return;
	}
}

inline static void S3D(glm::vec3* simplex, uint8_t* simplexSize, glm::vec3* v)
{
	glm::vec3 si, sj, sk;
	int testLineThree, testLineFour, testPlaneTwo, testPlaneThree, testPlaneFour, dotTotal;
	int i, j, k, t;
	int hff1_tests[3];

	glm::vec3 s1 = simplex[3];
	glm::vec3 s2 = simplex[2];
	glm::vec3 s3 = simplex[1];
	glm::vec3 s4 = simplex[0];

	glm::vec3 s1s2 = s2 - simplex[3];
	glm::vec3 s1s3 = s3 - simplex[3];
	glm::vec3 s1s4 = s4 - simplex[3];

	hff1_tests[2] = hff1(s1, s2);
	hff1_tests[1] = hff1(s1, s3);
	hff1_tests[0] = hff1(s1, s4);
	testLineThree = hff1(s1, s3);
	testLineFour = hff1(s1, s4);

	dotTotal = hff1(s1, s2) + testLineThree + testLineFour;
	if (dotTotal == 0)
	{
		S3Dregion1();
		return;
	}

	const float det134 = determinant(s1s3, s1s4, s1s2);
	const int sss = (det134 <= 0);

	testPlaneTwo = hff3(s1, s3, s4) - sss;
	testPlaneTwo = testPlaneTwo * testPlaneTwo;
	testPlaneThree = hff3(s1, s4, s2) - sss;
	testPlaneThree = testPlaneThree * testPlaneThree;
	testPlaneFour = hff3(s1, s2, s3) - sss;
	testPlaneFour = testPlaneFour * testPlaneFour;

	switch (testPlaneTwo + testPlaneThree + testPlaneFour)
	{
	case 3:
	{
		S3Dregion1234();
		break;
	}
	case 2:
	{
		// Only one facing the origin
		// 1,i,j, are the indices of the points on the triangle and remove k from

		*simplexSize = 3;
		if (!testPlaneTwo)
		{
			// k = 2;   removes s2
			simplex[2] = simplex[3];
		}
		else if (!testPlaneThree)
		{
			// k = 1; // removes s3
			simplex[1] = s2;
			simplex[2] = simplex[3];
		}
		else if (!testPlaneFour)
		{
			// k = 0; // removes s4  and no need to reorder
			simplex[0] = s3;
			simplex[1] = s2;
			simplex[2] = simplex[3];
		}
		S2D(simplex, simplexSize, v);
		break;
	}
	case 1:
	{
		// Two triangles face the origins:
		//    The only positive hff3 is for triangle 1,i,j, therefore k must be in
		//    the solution as it supports the the point of minimum norm.
		// 1,i,j, are the indices of the points on the triangle and remove k from

		*simplexSize = 3;
		if (testPlaneTwo)
		{
			k = 2; // s2
			i = 1;
			j = 0;
		}
		else if (testPlaneThree)
		{
			k = 1; // s3
			i = 0;
			j = 2;
		}
		else
		{
			k = 0; // s4
			i = 2;
			j = 1;
		}

		si = simplex[i];
		sj = simplex[j];
		sk = simplex[k];

		if (dotTotal == 1)
		{
			if (hff1_tests[k])
			{
				if (!hff2(s1, sk, si))
				{
					select_1ik();
					projectOnPlane(s1, si, sk, v);
				}
				else if (!hff2(s1, sk, sj))
				{
					select_1jk();
					projectOnPlane(s1, sj, sk, v);
				}
				else
				{
					select_1k(); // select region 1i
					projectOnLine(s1, sk, v);
				}
			}
			else if (hff1_tests[i])
			{
				if (!hff2(s1, si, sk))
				{
					select_1ik();
					projectOnPlane(s1, si, sk, v);
				}
				else
				{
					select_1i(); // select region 1i
					projectOnLine(s1, si, v);
				}
			}
			else
			{
				if (!hff2(s1, sj, sk))
				{
					select_1jk();
					projectOnPlane(s1, sj, sk, v);
				}
				else
				{
					select_1j(); // select region 1i
					projectOnLine(s1, sj, v);
				}
			}
		}

		else if (dotTotal == 2)
		{
			// Two edges have positive hff1, meaning that for two edges the origin's
			// project fall on the segement.
			//  Certainly the edge 1,k supports the the point of minimum norm, and so
			//  hff1_1k is positive

			if (hff1_tests[i])
			{
				if (!hff2(s1, sk, si))
				{
					if (!hff2(s1, si, sk))
					{
						select_1ik(); // select region 1ik
						projectOnPlane(s1, si, sk, v);
					}
					else
					{
						select_1k(); // select region 1k
						projectOnLine(s1, sk, v);
					}
				}
				else
				{
					if (!hff2(s1, sk, sj))
					{
						select_1jk(); // select region 1jk
						projectOnPlane(s1, sj, sk, v);
					}
					else
					{
						select_1k(); // select region 1k
						projectOnLine(s1, sk, v);
					}
				}
			}
			else if (hff1_tests[j])
			{
				//  I am not the villan in this story, I do what I do because there is no choice
				if (!hff2(s1, sk, sj))
				{
					if (!hff2(s1, sj, sk))
					{
						select_1jk();
						projectOnPlane(s1, sj, sk, v);
					}
					else
					{
						select_1j();
						projectOnLine(s1, sj, v);
					}
				}
				else
				{
					if (!hff2(s1, sk, si))
					{
						select_1ik();
						projectOnPlane(s1, si, sk, v);
					}
					else
					{
						select_1k();
						projectOnLine(s1, sk, v);
					}
				}
			}
			else
			{
				printf("GJK computed a degenerate case . . . ");
			}

		}
		else if (dotTotal == 3)
		{
			// sk is s.t. hff3 for sk < 0. So, sk must support the origin because
			// there are 2 triangles facing the origin.

			int hff2_ik = hff2(s1, si, sk);
			int hff2_jk = hff2(s1, sj, sk);
			int hff2_ki = hff2(s1, sk, si);
			int hff2_kj = hff2(s1, sk, sj);

			if (hff2_ki == 0 &&
				hff2_kj == 0)
			{
				//LOG_W("GJK, degenerate case went unhandled \n");
			}
			if (hff2_ki == 1 &&
				hff2_kj == 1)
			{
				select_1k();
				projectOnLine(s1, sk, v);
			}
			else if (hff2_ki)
			{
				// discard i
				if (hff2_jk)
				{
					// discard k
					select_1j();
					projectOnLine(s1, sj, v);
				}
				else
				{
					select_1jk();
					projectOnPlane(s1, sk, sj, v);
				}
			}
			else
			{
				// discard j
				if (hff2_ik)
				{
					// discard k
					select_1i();
					projectOnLine(s1, si, v);
				}
				else
				{
					select_1ik();
					projectOnPlane(s1, sk, si, v);
				}
			}
		}
		break;
	}
	case 0:
	{
		// The origin is outside all 3 triangles
		if (dotTotal == 1)
		{
			// Here si is set such that hff(s1,si) > 0
			if (testLineThree)
			{
				k = 2;
				i = 1; // s3
				j = 0;
			}
			else if (testLineFour)
			{
				k = 1; // s3
				i = 0;
				j = 2;
			}
			else
			{
				k = 0;
				i = 2; // s2
				j = 1;
			}

			si = simplex[i];
			sj = simplex[j];
			sk = simplex[k];

			if (!hff2(s1, si, sj))
			{
				select_1ij();
				projectOnPlane(s1, si, sj, v);
			}
			else if (!hff2(s1, si, sk))
			{
				select_1ik();
				projectOnPlane(s1, si, sk, v);
			}
			else
			{
				select_1i();
				projectOnLine(s1, si, v);
			}
		}
		else if (dotTotal == 2)
		{
			// Here si is set such that hff(s1,si) < 0
			*simplexSize = 3;
			if (!testLineThree)
			{
				k = 2;
				i = 1; // s3
				j = 0;
			}
			else if (!testLineFour)
			{
				k = 1;
				i = 0; // s4
				j = 2;
			}
			else
			{
				k = 0;
				i = 2; // s2
				j = 1;
			}

			si = simplex[i];
			sj = simplex[j];
			sk = simplex[k];

			if (!hff2(s1, sj, sk))
			{
				if (!hff2(s1, sk, sj))
				{
					select_1jk(); // select region 1jk
					projectOnPlane(s1, sj, sk, v);
				}
				else if (!hff2(s1, sk, si))
				{
					select_1ik();
					projectOnPlane(s1, sk, si, v);
				}
				else
				{
					select_1k();
					projectOnLine(s1, sk, v);
				}
			}
			else if (!hff2(s1, sj, si))
			{
				select_1ij();
				projectOnPlane(s1, si, sj, v);
			}
			else
			{
				select_1j();
				projectOnLine(s1, sj, v);
			}
		}
		break;
	}
	default:
	{
		printf("GJK entered a mangled state \n");
	}
	}
}

static inline uint32_t moveCollisionVerticies(glm::vec3* vert, uint16_t numVert, glm::vec3 deltaPos)
{
	// update with SIMD optimization
	for (int i = 0; i < numVert; ++i)
	{
		vert[i] += deltaPos;
	}
	return 0;
}

MxCollisionInstance* createMxCollision(const char* filePath, uint32_t flags, uint32_t maxObjects)
{
	MxCollisionInstance* newInst = (MxCollisionInstance*)calloc(1, sizeof(MxCollisionInstance*));
	if (newInst == NULL)
	{
		return NULL;
	}

	newInst->collisionBodies = (MxCollisionBody*)calloc(maxObjects, sizeof(MxCollisionBody));
	if (newInst->collisionBodies == NULL)
	{
		printf("FATAL: no mem, Failed to create MxCollisionBody \n");
	}

	uint16_t objectBitCount = maxObjects / 8;
	if (objectBitCount % 8)
	{
		objectBitCount += 1;
	}

	newInst->deltaBits = (uint8_t*)calloc(objectBitCount, sizeof(uint8_t));
	if (newInst->deltaBits == NULL)
	{
		printf("FATAL: no mem, Failed to create MxCollisionBody \n");
	}

	for (int i = 0; i < maxObjects; ++i)
	{
		newInst->collisionBodies[i].colId = i;
	}

	newInst->pirimitives = (MxAABB*)calloc(maxObjects, sizeof(MxAABB));
	if (newInst->pirimitives == NULL)
	{
		return NULL;
	}

	/*
	newInst->grid = (MxUniformGrid*)calloc(1, sizeof(MxUniformGrid));
	if (newInst->grid == NULL) 
	{
		return NULL;
	}
	*/

	newInst->instanceId		= clInstanceCounter++;
	newInst->numLoadedCols	= 0;
	newInst->flags			= flags;
	return newInst;
}

uint32_t initCollision(MxCollisionInstance* mx, uint32_t flags)
{
	for (int i = 0; i < mx->numLoadedCols; ++i)
	{
		//moveCollisionVerticies(objPtr->collisionVerticies, objPtr->vertexCount, deltaPos);
	}
	return 0;
}

//uint32_t updateCollisionObject(MxCollisionBody* obj, glm::vec3 delta, glm::quat orientation)
uint32_t updateCollisionObject(MxCollisionInstance* clInst, uint32_t colId, glm::vec3 delta, glm::quat orientation)
{
	// IDEA:
	// use a vector to keep changes all the changes in position until an object enters in sphere
	MxCollisionBody* objPtr = &clInst->collisionBodies[colId];
	//objPtr->centroid += delta;
	clInst->pirimitives[colId].centroid += delta;

	for (int i = 0; i < objPtr->vertexCount; ++i)
	{
		// need to account for rotation
		objPtr->collisionVerticies[i] += delta;
	}

	queryCollision(clInst, &clInst->pirimitives[colId]);

	return 0;
}

uint32_t createAABB(MxCollisionInstance* const mxInst, MxCollisionBody* objPtr, glm::vec3 centroid)
{
	//mxInst->numLoadedCols
	int maxX = 0, maxY = 0, maxZ = 0;
	float px = 0, py = 0, pz = 0;

	for (int i = 0; i < objPtr->vertexCount; ++i)
	{
		glm::vec3 recalculatedPosition = objPtr->collisionVerticies[i] - centroid;
		if (recalculatedPosition.x > px)
		{
			px = recalculatedPosition.x;
			maxX = i;
		}
		if (recalculatedPosition.y > py)
		{
			py = recalculatedPosition.y;
			maxY = i;
		}
		if (recalculatedPosition.z > pz)
		{
			pz = recalculatedPosition.z;
			maxZ = i;
		}
	}

	mxInst->pirimitives[objPtr->colId].centroid = centroid;
	mxInst->pirimitives[objPtr->colId].halfwith.x = px;
	mxInst->pirimitives[objPtr->colId].halfwith.y = py;
	mxInst->pirimitives[objPtr->colId].halfwith.z = pz;

	return objPtr->colId;
}

uint32_t updateCollision(MxCollisionInstance* mxInst)
{
	//
	for (int i = 0; i < mxInst->numLoadedCols; ++i)
	{
		//staticSphereIntersectionTest();
	}
	return 0;
}


float distanceQuery(glm::vec3* vert1, uint16_t obj1VertCount, glm::vec3* vert2, uint16_t obj2VertCount)
{
	glm::vec3 simplex[4];
	uint8_t simplexSize = 1;

	unsigned int k = 0;                /**< Iteration counter                 */
	const int mk = 25;                 /**< Maximum number of GJK iterations  */
	const float eps_rel = eps_rel22; /**< Tolerance on relative             */
	const float eps_tot = eps_tot22; /**< Tolerance on absolute distance    */

	const float eps_rel2 = eps_rel * eps_rel;
	unsigned int i;
	glm::vec3 w;
	glm::vec3 v;
	glm::vec3 vminus;
	float norm2Wmax = 0;

	/* Initialize search direction */
	v = vert1[0] - vert2[0];
	simplex[0] = v;

	do
	{
		k++;
		vminus = -v;
		w = vert1[support(vert1, obj1VertCount, vminus)] - vert2[support(vert2, obj2VertCount, v)];

		/* Test first exit condition (new point already in simplex/can't move further) */
		float exeedtol_rel = (norm2(v) - glm::dot(v, w));
		if (exeedtol_rel <= (eps_rel * norm2(v)) || exeedtol_rel < eps_tot22)
		{
			break;
		}

		if (norm2(v) < eps_rel2)
		{ // it a null V
			break;
		}

		simplex[simplexSize++] = w;

		/* Invoke distance sub-algorithm */
		switch (simplexSize)
		{
		case 4:
			S3D(simplex, &simplexSize, &v);
			break;
		case 3:
			S2D(simplex, &simplexSize, &v);
			break;
		case 2:
			S1D(simplex, &simplexSize, &v);
			break;
		default:
			printf("GJK simplex is in an undefined state");
		}

		/* Test */
		for (int jj = 0; jj < simplexSize; jj++)
		{
			float tesnorm = norm2(simplex[jj]);
			if (tesnorm > norm2Wmax)
			{
				norm2Wmax = tesnorm;
			}
		}

		if ((norm2(v) <= (eps_tot * eps_tot * norm2Wmax)))
		{
			break;
		}

	} while ((simplexSize != 4) && (k != mk));

	if (k == mk)
	{
		printf("GJK max number of iterations reached, exiting . . . ");
	}

	return sqrt(norm2(v));
}

uint32_t _debug_createBoundingBox(MxCollisionBody* objPtr, glm::vec3 center, glm::vec3 size)
{
	/*
	glm::vec3 diff = size - center;

	objPtr->vertexCount = 8;
	objPtr->centroid	= center;

	objPtr->collisionVerticies[0] = center + size;
	objPtr->collisionVerticies[1] = center + size * glm::vec3(-1, 1, 1);
	objPtr->collisionVerticies[2] = center + size * glm::vec3(1, -1, 1);
	objPtr->collisionVerticies[3] = center + size * glm::vec3(-1, -1, 1);
	objPtr->collisionVerticies[4] = center + size * glm::vec3(1, 1, -1);
	objPtr->collisionVerticies[5] = center + size * glm::vec3(-1, 1, -1);
	objPtr->collisionVerticies[6] = center + size * glm::vec3(1, -1, -1);
	objPtr->collisionVerticies[7] = center + size * glm::vec3(-1, -1, -1);

	printf("Object %i populated ", objPtr->colId);
	printf("Centered at %f, %f, %f \n", objPtr->centroid.x, objPtr->centroid.y, objPtr->centroid.z);
	for (int i = 0; i < 8; ++i)
	{
		printf("vertex %i, <%f, %f, %f> \n", i, objPtr->collisionVerticies[i].x
			, objPtr->collisionVerticies[i].y
			, objPtr->collisionVerticies[i].z);
	}
	return 0;
	*/
	return 0;
}

uint32_t createBVSphere(MxCollisionInstance* mxInst, MxCollisionBody* objPtr)
{
	/*
	sphereFromDistantPoints(objPtr);

	for (int i = 0; i < objPtr->vertexCount; ++i)
	{
		sphereOfSphereAndPoints(objPtr, i);
	}

	*/
	return 0;
}

uint32_t staticAABBTest(MxAABB* a, MxAABB* b)
{
	if (abs(a->centroid.x - b->centroid.x) > (a->halfwith.x + b->halfwith.x))
	{
		return 0;
	}
	if (abs(a->centroid.y - b->centroid.y) > (a->halfwith.y + b->halfwith.y))
	{
		return 0;
	}
	if (abs(a->centroid.z - b->centroid.z) > (a->halfwith.z + b->halfwith.z))
	{
		return 0;
	}
	return 1;
}

uint32_t staticSphereTest(MxSphere* a, MxSphere* b)
{
	glm::vec3 dist = a->centroid - b->centroid;
	float sqDist = dot(dist, dist);
	float rSum = 5.f;//vMag(collisionBodies[a].radius + collisionBodies[b].centroid);
	if ((rSum * rSum) > sqDist)
	{
		return 1;
	}
	return 0;
}

uint32_t buildUniformGrid(MxCollisionInstance* mx, glm::vec3 uCellCount, glm::vec3 uLengths, glm::vec3 uOrigin)
{
	uint32_t numX = 0, numY = 0, numZ = 0;

	mx->numCells = uCellCount;
	mx->axisLengths = uLengths;
	mx->gridOrigin = uOrigin;

	glm::vec3 subdiv_coeff = mx->numCells / mx->axisLengths;

	uint16_t countx = (uint16_t)mx->numCells.x;
	uint16_t county = (uint16_t)mx->numCells.y;
	uint16_t countz = (uint16_t)mx->numCells.z;

	numX = 2 * (int32_t)(mx->numCells.x);
	numY = 2 * (int32_t)(mx->numCells.y);
	numZ = 2 * (int32_t)(mx->numCells.z);
	///*

	printf("number of cells allocated %i, %i, %i, total size = %i \n", numX, numY, numZ, numX * numY * numZ);
	mx->gridObjects = (MxAABB****)calloc(numZ * numY * numX, sizeof(MxAABB));
	if (mx->gridObjects == NULL)
	{
		printf("Failed to create uniformGrid . . . exiting");
		return 0;
	}
	//

	for (int i = 0; i < numX; ++i)
	{
		mx->gridObjects = (MxAABB****)calloc(numX, sizeof(MxAABB****));
		for (int j = 0; j < numY; ++j)
		{
			mx->gridObjects[i] = (MxAABB***)calloc(numY, sizeof(MxAABB***));
			for (int k = 0; k < numZ; ++k)
			{
				mx->gridObjects[i][j] = (MxAABB**)calloc(numZ, sizeof(MxAABB**));
			}
		}
	}

	return 1;

}

uint32_t populateGrid(MxCollisionInstance* mx)
{
	glm::vec3 subdiv_coeff = mx->numCells / mx->axisLengths;

	uint16_t countx = (uint16_t)(2 * mx->numCells.x);
	uint16_t county = (uint16_t)(2 * mx->numCells.y);
	uint16_t countz = (uint16_t)(2 * mx->numCells.z);

	printf("Grid dimensions : %i, %i, %i \n", countx, county, countz);
	printf("Total num of cells = %i \n", countx * county * countz);

	mx->gridObjects = (MxAABB****)calloc(countx, 16);
	for (int i = 0; i < countx; ++i)
	{
		mx->gridObjects[i] = (MxAABB***)calloc(county, 16);
		for (int j = 0; j < county; ++j)
		{
			mx->gridObjects[i][j] = (MxAABB**)calloc(countz, 16);
			for (int k = 0; k < countz; ++k)
			{
				mx->gridObjects[i][j][k] = NULL;
			}
		}
	}

	for (int i = 0; i < mx->numLoadedCols; ++i)
	{
		glm::vec3 test = (mx->pirimitives[i].centroid - mx->gridOrigin) * subdiv_coeff;

		uint16_t axisx = (uint16_t)test.x + (uint16_t)mx->numCells.x;
		uint16_t axisy = (uint16_t)test.y + (uint16_t)mx->numCells.y;
		uint16_t axisz = (uint16_t)test.z + (uint16_t)mx->numCells.z;

		assert(axisx <= (uint16_t)(2 * mx->numCells.x));
		assert(axisy <= (uint16_t)(2 * mx->numCells.y));
		assert(axisz <= (uint16_t)(2 * mx->numCells.z));

		axisx = axisx % countx;
		axisy = axisy % county;
		axisz = axisz % countz;

		printf("object %i, added to cell, %i, %i, %i \n", i, axisx, axisy, axisz);

		if (mx->gridObjects[axisx][axisy][axisz] == NULL)
		{
			mx->gridObjects[axisx][axisy][axisz] = &mx->pirimitives[i];
			mx->pirimitives[i].next = NULL;
		}
		else
		{
			printf("");
			mx->pirimitives[i].next = mx->gridObjects[axisx][axisy][axisz];
			mx->gridObjects[axisx][axisy][axisz] = &mx->pirimitives[i];
		}
	}
	
	return 0;
}


uint32_t updateObjectInGrid(MxCollisionInstance* mx, MxAABB* obj, glm::vec3 dLinear, glm::vec3 dAngular)
{
	MxAABB* prev = NULL;
	MxAABB* cursor = NULL;
	uint16_t iX, iY, iZ;

	glm::vec3 testCurrent = (obj->centroid - mx->gridOrigin + obj->halfwith) * (mx->numCells / mx->axisLengths);
	glm::vec3 testNext = ((obj->centroid + dLinear) - mx->gridOrigin + obj->halfwith) * (mx->numCells / mx->axisLengths);


	if ((uint16_t)testCurrent.x != (uint16_t)testNext.x &&
		(uint16_t)testCurrent.y != (uint16_t)testNext.y &&
		(uint16_t)testCurrent.z != (uint16_t)testNext.z)
	{
		printf("Object moved to cell <%i, %i, %i>\n", (uint16_t)testNext.x, (uint16_t)testNext.y, (uint16_t)testNext.z);

		iX = testCurrent.x;
		iY = testCurrent.y;
		iZ = testCurrent.z;
		// remove refernce from last cell
		for (cursor = mx->gridObjects[iX][iY][iZ];
			cursor != NULL;
			cursor = cursor->next)
		{
			if (cursor == obj)
			{

			}
			prev = cursor;
		}

		// add reference to new cell
	}
	return 0;
}

uint16_t queryCollision(MxCollisionInstance* mx, MxAABB* obj)
{
	MxAABB* cursor;
	uint16_t cX = 0, cY = 0, cZ = 0;
	uint16_t numContacts = 0;

	glm::vec3 subdiv_coeff = mx->numCells / mx->axisLengths;

	glm::vec3 testHi = (obj->centroid - mx->gridOrigin + obj->halfwith) * subdiv_coeff;
	glm::vec3 testLo = (obj->centroid - mx->gridOrigin - obj->halfwith) * subdiv_coeff;
	glm::vec3 test = (obj->centroid - mx->gridOrigin) * subdiv_coeff;

	cX = (uint16_t)test.x + (uint16_t)mx->numCells.x;
	cY = (uint16_t)test.y + (uint16_t)mx->numCells.y;
	cZ = (uint16_t)test.z + (uint16_t)mx->numCells.z;

	assert(cX <= (uint16_t)(2 * mx->numCells.x));
	assert(cY <= (uint16_t)(2 * mx->numCells.y));
	assert(cZ <= (uint16_t)(2 * mx->numCells.z));

	cX = cX % (uint16_t)(2 * mx->numCells.x);
	cY = cY % (uint16_t)(2 * mx->numCells.y);
	cZ = cZ % (uint16_t)(2 * mx->numCells.z);

	if ((uint16_t)testHi.x == (uint16_t)testLo.x ||
		(uint16_t)testHi.y == (uint16_t)testLo.y ||
		(uint16_t)testHi.z == (uint16_t)testLo.z)
	{
		for (cursor = mx->gridObjects[cX][cY][cZ];
			cursor != NULL;
			cursor = cursor->next)
		{
			if (cursor == obj)
			{
				continue;
			}

			if (staticAABBTest(cursor, obj))
			{
				uint64_t addressA = ((uint64_t)cursor - (uint64_t)(&mx->pirimitives[0])) / sizeof(MxAABB);
				uint64_t addressB = ((uint64_t)obj - (uint64_t)(&mx->pirimitives[0])) / sizeof(MxAABB);
				//printf("Objects %i, %i are colliding \n", addressA, addressB);
				pushCollision(mx->contactQueue, addressA, addressB);
				numContacts++;
			}
		}
	}
	else
	{
		for (int i = 0; i < 9; ++i)
		{
			cX = cX + (1 >> 2);
			for (cursor = mx->gridObjects[cX][cY][cZ];
				cursor != NULL;
				cursor = cursor->next)
			{
				if (obj != cursor)
				{
					continue;
				}

				if (staticAABBTest(cursor, obj) )
				{
					uint64_t addressA = ((uint64_t)cursor - (uint64_t)(&mx->pirimitives[0])) / sizeof(MxAABB);
					uint64_t addressB = ((uint64_t)obj - (uint64_t)(&mx->pirimitives[0])) / sizeof(MxAABB);
					//printf("Objects %i, %i are colliding \n", addressA, addressB);
					pushCollision(mx->contactQueue, addressA, addressB);
					numContacts++;
				}
			}
		}
	}

	return numContacts;
}