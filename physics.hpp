#include <iostream>

#ifndef PHYSICS_H
#define PHYSICS_H

#include "physics_types.h"

#define GLM_FORCE_RADIANS
#define GLM_FORCE_DEPTH_ZERO_TO_ONE
#define GLM_ENABLE_EXPERIMENTAL
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/quaternion.hpp>
#include <glm/gtx/hash.hpp>
#include <glm/gtx/euler_angles.hpp>

static uint8_t mxInstanceCounter = 0;


MechanixInstance* const createMechanixInstance(const char* filepath, uint32_t flags, uint32_t maxObjects);

uint32_t loadMechanixObject(MechanixInstance* mxInst, const char* filePath);
uint32_t bindMxRigidBody(MechanixInstance* mxInst, FbGameObject* entity, uint32_t phyId, EntityID id);

uint32_t initPhysics(MechanixInstance* mxInst, uint32_t flags);
uint32_t updatePhysics(MechanixInstance* mxInst, float timeStep);
uint32_t updateSinglePhysics(MechanixInstance* mxInst, uint32_t objectId, float timeStep);
uint32_t resolveContacts(MechanixInstance* mxInst);

uint32_t addForceToObject(MechanixInstance* mxInst, uint32_t objId, MxForce* force);
uint32_t removeForceFromObject(MechanixInstance* mxInst, uint32_t objId, MxForce* force);

// configurable paramters
#define MX_MAX_COLLISIONS_PER_FRAME	255

// type flags
#define MX_FLAGS_DEBUG_MODE		0x00000001
#define MX_RENDER_COLL_VERT		0x00000002
#define MX_INSTANCE_SHOULD_LIVE	0x00000004
#define MX_INSTANCE_NO_FB_LINK	0x00000008


// return values
#define MX_FAILED_TO_LOAD 0xFFFFFFFF

// DEBUG ONLY

#endif