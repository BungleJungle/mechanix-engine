#pragma once
#include "iostream"

#include "physics.hpp"
#include "collision_types.h"
#include "physics_types.h"

uint32_t createCollisionQueue(Contacts* ct)
{
	ct->contact = (ContactNode*)calloc(MX_MAX_COLLISIONS_PER_FRAME, sizeof(ContactNode));
	ct->countContacts = 0;

	if (ct->contact == NULL)
	{
		return MX_FAILED_TO_LOAD;
	}
	return 0;
}

uint32_t destroyCollisionQueue(Contacts* ct)
{
	free(ct->contact);
	return 0;
}

uint32_t pushCollision(Contacts* ct, uint32_t objA, uint32_t objB)
{
	if (ct->countContacts >= MX_MAX_COLLISIONS_PER_FRAME)
	{
		return MX_FAILED_TO_LOAD;
	}

	ContactNode* temp = &ct->contact[ct->countContacts++];
	temp->a = objA;
	temp->b = objB;

	return 0;
}

ContactNode* popCollision(Contacts* ct)
{
	assert(ct->countContacts > 0);
	ContactNode* temp = NULL;

	if (ct->countContacts)
	{
		temp = &ct->contact[--ct->countContacts];
	}

	return temp;
}
