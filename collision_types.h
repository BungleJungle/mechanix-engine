#pragma once

#include "stdlib.h"

#ifndef COLLISION_TYPES_H
#define COLLISION_TYPES_H

#define GLM_FORCE_RADIANS
#define GLM_FORCE_DEPTH_ZERO_TO_ONE
#define GLM_ENABLE_EXPERIMENTAL
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/quaternion.hpp>
#include "contact_queue.h"

struct MxSphere
{
	float r;
	glm::vec3 centroid;
};

struct MxAABB
{
	glm::vec3 halfwith;
	glm::vec3 centroid;
	MxAABB* next;
};

struct ContactNode
{
	//MxAABB* b;
	//MxAABB* a;
	uint32_t a;
	uint32_t b;
};

struct Contacts
{
	ContactNode* contact;
	uint32_t	 countContacts;
};

struct MxCollisionBody
{
	glm::vec3* collisionVerticies;
	uint32_t	masterId;
	uint32_t 	colId;
	uint32_t 	vertexCount : 10;
	uint32_t	flags : 14;

	//uint32_t		flags		:	14;
#define COL_BODY_		0x0001
};

struct MxCollisionInstance
{
	MxCollisionBody* collisionBodies;

	// uniform grid data
	glm::vec3		numCells;
	glm::vec3		axisLengths;
	glm::vec3		gridOrigin;
	MxAABB****		gridObjects;

	// contact resolution data
	Contacts*		contactQueue;
	MxAABB*			pirimitives;
	uint8_t*		deltaBits;
	uint16_t		numContacts;
	glm::vec3		gravityVector;
	uint32_t		flags;
	uint32_t		numLoadedCols;
	uint32_t		maxCols;
	uint8_t			instanceId;

};




#endif
