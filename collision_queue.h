#pragma once

#ifndef COLLISION_QUEUE_H
#define COLLISION_QUEUE_H

#include "collision_types.h"

uint32_t		createCollisionQueue(Contacts* ct);
uint32_t		destroyCollisionQueue(Contacts* ct);
uint32_t		pushCollision(Contacts* ct, uint32_t objA, uint32_t objB);
ContactNode*	popCollision(Contacts* ct);

#endif // type