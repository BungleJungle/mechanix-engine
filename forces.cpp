#include "physics.hpp"

static inline MxForce* createForce()
{
	MxForce* temp = (MxForce*)calloc(1, sizeof(MxForce));
	
}

uint32_t addForceToObject(MechanixInstance* mxInst, uint32_t objId, MxForce* force)
{
	// TODO : replace with faster allocation

	// TODO: have a better method for doing this
	assert(objId < mxInst->maxBodies);
	assert(force != NULL);

	MxRigidBody* object = &mxInst->rigidBodies[objId];

	object->numForces += 1;

	if (object->forces != NULL)
	{
		force->next = object->forces;
		object->forces = force;
	}
	else
	{
		force->next = NULL;
		object->forces = force;
	}

	return 0;
}

uint32_t removeForceFromObject(MechanixInstance* mxInst, uint32_t objId, MxForce* force)
{
	assert(objId < mxInst->maxBodies);
	assert(force != NULL);
	uint32_t numForces = mxInst->rigidBodies[objId].numForces--;

	//

	return numForces;
}