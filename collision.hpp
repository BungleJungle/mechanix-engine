#include <iostream>

#ifndef COLLISION_H
#define COLLISION_H

#include "collision_types.h"

#define GLM_FORCE_RADIANS
#define GLM_FORCE_DEPTH_ZERO_TO_ONE
#define GLM_ENABLE_EXPERIMENTAL
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/quaternion.hpp>
#include <glm/gtx/hash.hpp>
#include <glm/gtx/euler_angles.hpp>

static uint8_t clInstanceCounter = 0;

#define ZERO 1e-20
#define CHECK_ZERO (flt) (flt >= -1e-20 && flt <= 1e-20)

#define CL_FLAGS_DEBUG_MODE 0x01



uint32_t	staticSphereTest(MxSphere* a, MxSphere* b);
uint32_t	staticAABBTest(MxAABB* a, MxAABB* b);
float		distanceQuery(glm::vec3* vert1, uint16_t obj1VertCount, glm::vec3* vert2, uint16_t obj2VertCount);

MxCollisionInstance* createMxCollision(const char* filePath, uint32_t flags, uint32_t maxObjects);

uint32_t	initCollision(MxCollisionInstance* mx, uint32_t flags);
uint32_t	updateCollisionObject(MxCollisionInstance* clInst, uint32_t colId, glm::vec3 delta, glm::quat orientation);

//			Collision primitives creation
uint32_t	createAABB(MxCollisionInstance* const mxInst, MxCollisionBody* objPtr, glm::vec3 centroid);
uint32_t	createBVSphere(MxCollisionInstance* mxInst, MxCollisionBody* objPtr);

//			Bouding volume hierarchies
uint16_t	queryCollision(MxCollisionInstance* mx, MxAABB* obj);
uint32_t	updateObjectInGrid(MxCollisionInstance* mx, MxAABB* obj, glm::vec3 dLinear, glm::vec3 dAngular);
uint32_t	populateGrid(MxCollisionInstance* mx);
uint32_t	buildUniformGrid(MxCollisionInstance* mx, glm::vec3 uCellCount, glm::vec3 uLengths, glm::vec3 uOrigin);


// Collision processing, and response
//uint32_t	queryCollision(uint32_t id);

// debug tools only
uint32_t	_debug_createBoundingBox(MxCollisionBody* objPtr, glm::vec3 center, glm::vec3 size);


#endif