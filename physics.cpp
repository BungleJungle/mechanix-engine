#include <assert.h>
#include <math.h>
#include <string.h>
#include <iostream>
#include <string>
#include <regex>
#include <cstdlib>
#include <stdio.h>
#include <fstream>

#include "physics.hpp"
#include "collision.hpp"
#include "physics_types.h"
#include "entity.hpp"
#include "collision_types.h"
#include "collision_queue.h"
/*
	PHYSICS API
*/

#define PRINT_VEC(vec, vecName) printf("%s = <%f, %f, %f>\n", vecName, vec.x, vec.y, vec.z );

// fuck you visual studio
#pragma warning(disable : 4996)

#define ANGULAR_POSITION_DEF	0x0001
#define ANGUALR_VELCOITY_DEF	0x0002
#define FRICTION_COEFF_DEF		0x0004
#define ELASTIC_COEFF_DEF		0x0008
#define MASS_DEF				0x0010
#define FLAGS_DEF				0x0020
#define POSITION_DEF			0x0040
#define VELOCITY_DEF			0x0080
#define VERTEX_COUNT_DEF		0x0100
#define VERTEX_DATA_DEF			0x0200
#define VERTEX_DATA_DONE		0x0400
#define UNMARKED_VARIABLE		0x0800
#define SUCCESSFUL_LOAD			0x07FF

#define MAX_LINE_SIZE			255

static glm::vec3 extractVector(char* line)
{
	assert(line != NULL);
	assert(line[0] != '\0');
	glm::vec3 vecFromText = glm::vec3(0,0,0);
	line = strtok(NULL, ",");
	vecFromText.x = atof(line);
	line = strtok(NULL, ",");
	vecFromText.y = atof(line);
	line = strtok(NULL, ",");
	vecFromText.z = atof(line);
	return vecFromText;
}

MechanixInstance* const createMechanixInstance(const char* filepath, uint32_t flags, uint32_t maxObjects)
{
	uint32_t colFlags = 0;
	MechanixInstance* newInst = (MechanixInstance*)calloc(1, sizeof (MechanixInstance));
	if (newInst == NULL)
	{
		return NULL;
	}

	newInst->rigidBodies = (MxRigidBody*)calloc(maxObjects, sizeof(MxRigidBody));
	if (newInst->rigidBodies == NULL)
	{
		printf("FATAL: no mem, Failed to allocate MechanixInstance variables \n");
		return NULL;
	}

	newInst->deltaBits = (uint8_t*)calloc((maxObjects / 8 + 1), sizeof(uint8_t));
	if (newInst->deltaBits == NULL)
	{
		printf("no mem, failed to create MechanixInstance \n");
		return NULL;
	}

	if (flags & MX_FLAGS_DEBUG_MODE)
	{
		newInst->gravtiyVector = glm::vec3(0, -9.81 / 2.0f, 0);
		colFlags |= MX_FLAGS_DEBUG_MODE | MX_RENDER_COLL_VERT;
		printf("Mechanix Instance running in debug mode \n");
	}

	newInst->contactQueue = (Contacts*)calloc(1, sizeof(Contacts));
	if (newInst->contactQueue == NULL)
	{
		return NULL;
	}

	createCollisionQueue(newInst->contactQueue);

	newInst->colInst		= createMxCollision(NULL, colFlags, 16);
	newInst->maxBodies		= maxObjects;
	newInst->instanceId		= mxInstanceCounter++;
	newInst->instanceFlags	= flags;
	newInst->instanceFlags |= MX_INSTANCE_SHOULD_LIVE;
	newInst->colInst->contactQueue = newInst->contactQueue;

	return newInst;
}

uint32_t loadMechanixObject(MechanixInstance* mxInst, const char* filePath)
{
	FILE *fp;
	MxRigidBody* obj;
	MxCollisionBody* colObj;
	uint32_t objId = 0;
	uint16_t currentValue = 0;
	uint16_t valuesSet = 0;
	uint16_t vertexCount = 0;
	uint16_t indexCounter = 0;
	glm::vec3* vecData = NULL;
	glm::vec3 vecFromText(0, 0, 0);
	glm::vec3 centroid(0, 0, 0);
	float floatFromText = 0;
	char line[256];

	assert(mxInst != NULL);
	assert(mxInst->colInst != NULL);
	assert(filePath != NULL);

	if (mxInst->numLoadedBodies >= mxInst->maxBodies)
	{
		printf("Not enough memory allocated for object file %s \n", filePath);
		return MX_FAILED_TO_LOAD;
	}

	fp = fopen(filePath, "r");
	if (fp == NULL)
	{
		printf("File pointer was NULL\n");
		return MX_FAILED_TO_LOAD;
	}
	
	objId = mxInst->numLoadedBodies++;
	obj = &mxInst->rigidBodies[objId];
	char* parsedStr = NULL;

	while (fgets(line, MAX_LINE_SIZE, fp))
	{
		if (line[0] != '\0')
		{
			parsedStr = strtok(line, "=");
		}

		while (parsedStr != NULL)
		{
			if (parsedStr == NULL) 
			{
				continue;
			}
			else if (!strcmp(parsedStr, "position"))
			{
				valuesSet |= POSITION_DEF;
				//obj->position = extractVector(parsedStr);
				//PRINT_VEC(obj->position, "Position");
			}
			else if (!strcmp(parsedStr, "velocity"))
			{
				valuesSet |= VELOCITY_DEF;
				obj->velocity = extractVector(parsedStr);
				//PRINT_VEC(obj->velocity, "velocity");
			}
			else if (!strcmp(parsedStr, "angularPosition"))
			{
				valuesSet |= ANGULAR_POSITION_DEF;
				obj->angularPosition = extractVector(parsedStr);
				//PRINT_VEC(obj->angularPosition, "angularPosition");
			}
			else if (!strcmp(parsedStr, "angularVelocity"))
			{
				valuesSet |= ANGUALR_VELCOITY_DEF;
				obj->angularVelocity = extractVector(parsedStr);
				//PRINT_VEC(obj->angularVelocity, "angularVelocity");
			}
			else if (!strcmp(parsedStr, "frictionCoeff"))
			{
				valuesSet |= FRICTION_COEFF_DEF;
				parsedStr = strtok(NULL, "=");

				obj->frictionCoeff = atof(parsedStr);
				//printf("friction coeff = %f \n", obj->frictionCoeff);
			}
			else if (!strcmp(parsedStr, "elasticCoeff"))
			{
				valuesSet |= ELASTIC_COEFF_DEF;
				parsedStr = strtok(NULL, "=");

				obj->elasticCoeff = atof(parsedStr);
				//printf("elasticCoeff = %f \n", obj->elasticCoeff);
			}
			else if (!strcmp(parsedStr, "mass"))
			{
				valuesSet |= MASS_DEF;
				parsedStr = strtok(NULL, "=");

				obj->invMass = 1.0f / atof(parsedStr);
				//printf("inverse mass = %f \n", obj->invMass);
			}
			else if (!strcmp(parsedStr, "flags"))
			{
				valuesSet |= FLAGS_DEF;
				parsedStr = strtok(NULL, "=");

				obj->flags = atoi(parsedStr);
				//printf("flags = %li\n", obj->flags);
			}
			else if (!strcmp(parsedStr, "vertexCount"))
			{
				valuesSet |= VERTEX_COUNT_DEF;
				parsedStr = strtok(NULL, "=");
				vertexCount = (uint16_t)atoi(parsedStr);
			}
			else if (!strcmp(parsedStr, "vertexData"))
			{
				valuesSet |= VERTEX_DATA_DEF;
				parsedStr = strtok(NULL, "=");
				if (vertexCount)
				{
					vecData = (glm::vec3*)calloc(vertexCount, sizeof(glm::vec3));
				}
				else
				{
					printf("ERROR: vertex data not properly set \n");
				}
			}
			else if (!strcmp(parsedStr, "]"))
			{
				valuesSet |= VERTEX_DATA_DONE;
				centroid /= (float)vertexCount;
				printf("\n");
			}
			else 
			{
				if ( (valuesSet & VERTEX_DATA_DEF) &&
					 !(valuesSet & VERTEX_DATA_DONE))
				{
					if (strcmp(parsedStr, "[") && 
						parsedStr[0] != '\0')
					{
						parsedStr = strtok(parsedStr, ",");
						vecFromText.x = atof(parsedStr);
						parsedStr = strtok(NULL, ",");
						vecFromText.y = atof(parsedStr);
						parsedStr = strtok(NULL, ",");
						vecFromText.z = atof(parsedStr);
						vecData[indexCounter] = vecFromText;
						centroid += vecData[indexCounter];
						printf("vec %i = <%f, %f, %f> \n", indexCounter, vecData[indexCounter].x, vecData[indexCounter].y, vecData[indexCounter].z);
						indexCounter++;
					}
				}
				else
				{
					valuesSet |= UNMARKED_VARIABLE; 
					printf("data value = %s \n", parsedStr);
					printf("Unassinged variabel in file %s \n", filePath);
				}
			}

			if (parsedStr[0] != '\0')
			{
				parsedStr = strtok(NULL, "=");
			}
		}
	}
	
	if ( (valuesSet & SUCCESSFUL_LOAD) == SUCCESSFUL_LOAD)
	{
		obj->position				= centroid;

		colObj						= &mxInst->colInst->collisionBodies[mxInst->colInst->numLoadedCols];
		colObj->collisionVerticies	= vecData;
		colObj->vertexCount			= vertexCount;
		colObj->colId				= mxInst->colInst->numLoadedCols;
		colObj->masterId			= obj->physId;
		mxInst->colInst->pirimitives[mxInst->colInst->numLoadedCols].centroid = centroid;
		createAABB(mxInst->colInst, colObj, centroid);
		/*
		glm::vec3 temp = mxInst->colInst->pirimitives[mxInst->colInst->numLoadedCols].halfwith;
		printf("Halfwiths = %f, %f, %f \n", temp.x, temp.y, temp.z);
		temp = mxInst->colInst->pirimitives[mxInst->colInst->numLoadedCols].centroid;
		printf("Centroid  = %f, %f, %f \n", temp.x, temp.y, temp.z);
		*/
		obj->colSlave = mxInst->colInst->numLoadedCols++;
		return objId;
	} 
	return MX_FAILED_TO_LOAD;
}

// used locallly, we no longer need them
#undef ANGULAR_POSITION_DEF
#undef ANGUALR_VELCOITY_DEF
#undef FRICTION_COEFF_DEF
#undef ELASTIC_COEFF_DEF
#undef MASS_DEF			
#undef FLAGS_DEF		
#undef POSITION_DEF		
#undef VELOCITY_DEF		
#undef VERTEX_COUNT_DEF	
#undef VERTEX_DATA_DEF	
#undef VERTEX_DATA_DONE	
#undef UNMARKED_VARIABLE

uint32_t bindMxRigidBody(MechanixInstance* mxInst, FbGameObject* entity, uint32_t phyId, EntityID id)
{
	glm::vec3 diffDist = glm::vec3(0, 0, 0);
	mxInst->rigidBodies[phyId].colSlave = mxInst->colInst->numLoadedCols;
	MxCollisionBody* colObj = (MxCollisionBody*)&mxInst->colInst->collisionBodies[mxInst->rigidBodies[phyId].colSlave];

	if (entity->numComponents > entity->maxComponents)
	{
		printf("Too many objects bound to GameObject \n");
		return -1;
	}

	entity->components[entity->numComponents] = (void*)&mxInst->rigidBodies[phyId];
	entity->compType[entity->numComponents++] = (uint8_t)id;
	mxInst->rigidBodies[phyId].masterObj = entity;

	if (mxInst->rigidBodies[phyId].flags & RIGID_NO_COLLISION)
	{
		return 0;
	}

	diffDist = mxInst->rigidBodies[phyId].position - entity->worldPosition;
	mxInst->rigidBodies[phyId].position = entity->worldPosition;

	for (int i = 0; i < colObj->vertexCount; ++i)
	{
		colObj->collisionVerticies[i] += diffDist;
	}

	return 0;
}

uint32_t updatePhysics(MechanixInstance* mxInst, float timeStep)
{
	glm::vec3 calcAccel;
	glm::vec3 calcAngularAccel;
	MxForce* prev = NULL;

	uint8_t moveStatus = 0;

	for (int objId = 0; objId < mxInst->numLoadedBodies; ++objId)
	{
		printf("Num Forces = %i \n", mxInst->rigidBodies[objId].numForces);

		calcAccel = glm::vec3(0, 0, 0);
		calcAngularAccel = glm::vec3(0, 0, 0);

		moveStatus = (mxInst->deltaBits[(int)objId / 8] & (1 << objId % 8));
		moveStatus >>= (objId % 8);

		switch (moveStatus)
		{
			case OBJECT_NO_MOVEMENT:
			{
				break;
			}
			case OBJECT_WILL_MOVE:
			{
				mxInst->deltaBits[(int)objId / 8] |= (objId % 8);

				for (MxForce* cursor = mxInst->rigidBodies[objId].forces;
					cursor != NULL; 
					cursor = cursor->next)
				{
					// TODO: account for angular forces
					calcAccel += cursor->linear;
					cursor->duration -= timeStep;

					if (cursor->duration <= timeStep)
					{
						printf("Force removed \n");
						mxInst->rigidBodies[objId].numForces -= 1;

						if (prev != NULL)
						{
							if (cursor->next != NULL)
							{
								prev->next = cursor->next;
								cursor = prev;
							}
							else
							{
								prev->next = NULL;
								free(cursor);
								break;
							}
						}
						else
						{
							if (cursor->next != NULL)
							{
								mxInst->rigidBodies[objId].forces = cursor->next;
								// TODO: this case will skip remaing forces
								break;
							}
							else
							{
								mxInst->rigidBodies[objId].numForces = 0;
								mxInst->rigidBodies[objId].forces = NULL;
								free(cursor);
								break;
							}
						}

						free(cursor);
					}
					else
					{
						prev = cursor;
					}
				}

				calcAccel *= mxInst->rigidBodies[objId].invMass;
				mxInst->rigidBodies[objId].velocity += calcAccel * timeStep;
				mxInst->rigidBodies[objId].position += mxInst->rigidBodies[objId].velocity * timeStep;

				updateCollisionObject(mxInst->colInst, 
					mxInst->rigidBodies[objId].colSlave,
					mxInst->rigidBodies[objId].velocity * timeStep,
					glm::vec3(0, 0, 0));

				break;
			}
			default:
			{
				printf("object forces data in a mangled state \n");
				break;
			}
		}
	}

	return 0;
}

uint32_t updateSinglePhysics(MechanixInstance* mxInst, uint32_t objId, float timeStep)
{
	glm::vec3 linearAcc = glm::vec3(0, 0, 0);
	glm::vec3 angularAccel = glm::vec3(0, 0, 0);
	MxForce* prev = NULL;

	if (mxInst->rigidBodies[objId].forces != NULL)
	{
		printf("Num Forces = %i \n", mxInst->rigidBodies[objId].numForces);

		for (MxForce* cursor = mxInst->rigidBodies[objId].forces;
			 cursor != NULL;
			 cursor = cursor->next)
		{
			// TODO: account for angular forces
			linearAcc += cursor->linear;
			cursor->duration -= timeStep;

			if (cursor->duration <= timeStep)
			{
				printf("Force removed \n");
				mxInst->rigidBodies[objId].numForces -= 1;

				if (prev != NULL)
				{
					if (cursor->next != NULL)
					{
						prev->next = cursor->next;
					}
					else
					{
						prev->next = NULL;
					}
				}
				else
				{
					if (cursor->next != NULL)
					{
						mxInst->rigidBodies[objId].forces = cursor->next;
						break;
					}
					else
					{
						mxInst->rigidBodies[objId].numForces = 0;
						mxInst->rigidBodies[objId].forces = NULL;
					}
				}

				free(cursor);
			}
			else
			{
				prev = cursor;
			}
		}

		linearAcc *= mxInst->rigidBodies[objId].invMass;
		mxInst->rigidBodies[objId].velocity += linearAcc * timeStep;
		mxInst->rigidBodies[objId].position += mxInst->rigidBodies[objId].velocity * timeStep;

	}
	else
	{
		linearAcc = angularAccel = glm::vec3(0, 0, 0);
	}

	updateCollisionObject(mxInst->colInst, 
		mxInst->rigidBodies[objId].colSlave, 
		mxInst->rigidBodies[objId].velocity * timeStep, 
		angularAccel);

	mxInst->rigidBodies[objId].masterObj->worldPosition += mxInst->rigidBodies[objId].velocity * timeStep;
	return 0;
}

uint32_t initPhysics(MechanixInstance* mxInst, uint32_t flags)
{
	for (int i = 0; i < mxInst->numLoadedBodies; ++i)
	{
		if (!(mxInst->rigidBodies[i].flags & RIGID_NO_GRAVITY))
		{
			//LOG_I("ADDING GRACITY VECTOR \n");
			MxForce* temp = (MxForce*)calloc(1, sizeof(MxForce));
			temp->angular = glm::vec3(0,0,0);
			temp->duration = std::numeric_limits<float>::infinity();
			temp->linear = mxInst->gravtiyVector * mxInst->rigidBodies[i].invMass;
			temp->next = NULL;
			addForceToObject(mxInst, i, temp);

			// deltaBits indicate what objects will be moving next frame
			mxInst->deltaBits[i / 8] |= (1 << i % 8);
		}

		if (!(flags & MX_INSTANCE_NO_FB_LINK))
		{
			if (mxInst->rigidBodies[i].masterObj)
			{
				mxInst->rigidBodies[i].position = mxInst->rigidBodies[i].masterObj->worldPosition;
			}
			else
			{
				printf("Object ID : %i, does not have a master object \n", i);
			}
		}
	}

	if (flags & MX_FLAGS_DEBUG_MODE)
	{
		initCollision(mxInst->colInst, MX_FLAGS_DEBUG_MODE);

		for (int i = 0; i < 3; ++i)
		{
			createBVSphere(mxInst->colInst, &mxInst->colInst->collisionBodies[i]);
		}

		for (int i = 0; i < 3; ++i)
		{
			//printf("Object %i, with a BV radius of %f centered at %f, %f, %f \n", i, mxInst->colInst->collisionBodies[i].radius,
			//	mxInst->colInst->collisionBodies[i].centroid.x, mxInst->colInst->collisionBodies[i].centroid.y, mxInst->colInst->collisionBodies[i].centroid.z);
		}

		//initUniformGrid(glm::vec3(0, 0, 0), glm::vec3(1000, 1000, 1000), glm::vec3(10, 10, 10));
	}
	else
	{
		initCollision(mxInst->colInst, NULL);
	}


	return 0;
}

uint32_t resolveContacts(MechanixInstance* mxInst)
{
	for (int i = mxInst->contactQueue->countContacts; i != 0; --i)
	{
		ContactNode* collision = popCollision(mxInst->contactQueue);

		// elastic collision equation
		// M1*V1 + M2*V2 = M1*V1' + M2*V2'
		MxCollisionBody* tempA = &mxInst->colInst->collisionBodies[collision->a];
		MxCollisionBody* tempB = &mxInst->colInst->collisionBodies[collision->b];

		float distance = distanceQuery(tempA->collisionVerticies,
				tempA->vertexCount,
				tempB->collisionVerticies,
				tempB->vertexCount);

		//printf("Distance = %f \n", distance);
		if (distance < ZERO)
		{
			printf("Obejct %i, %i are colliding \n", collision->a, collision->b);
			MxRigidBody* rigidA = &mxInst->rigidBodies[tempA->masterId];
			MxRigidBody* rigidB = &mxInst->rigidBodies[tempB->masterId];

			float massA = (1.0f / rigidA->invMass);
			float massB = (1.0f / rigidB->invMass);

			MxForce* forceResponse = (MxForce*)calloc(1, sizeof(MxForce));

			// Force applied to object A:
			forceResponse->linear = (2 * (massB / massA + massB)) * rigidB->velocity + ((massB - massA / massA + massB) * rigidA->velocity);
			forceResponse->angular = glm::vec3(0,0,0);	// TODO: account for angular forces
			forceResponse->duration = 0.030f;				// TODO: calculate proper impules
			addForceToObject(mxInst, tempA->masterId, forceResponse);


			forceResponse = (MxForce*)calloc(1, sizeof(MxForce));
			// Force applied to object B:
			forceResponse->linear = (2 * (massA / massA + massB)) * rigidA->velocity + ((massA - massB / massA + massB) * rigidB->velocity);
			forceResponse->angular = glm::vec3(0, 0, 0);	// TODO: account for angular forces
			forceResponse->duration = 0.030f;				// TODO: calculate proper impules
			addForceToObject(mxInst, tempB->masterId, forceResponse);
		}
	}
	printf("ENDED \n");

	return 0;
}

