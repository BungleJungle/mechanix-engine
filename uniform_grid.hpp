#pragma once

#include <iostream>

#ifndef UNIFORM_GRID_H
#define UNIFORM_GRID_H

#define GLM_FORCE_RADIANS
#define GLM_FORCE_DEPTH_ZERO_TO_ONE
#define GLM_ENABLE_EXPERIMENTAL
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/quaternion.hpp>

#include "collision_types.h"

class MxUniformGrid
{
	MxAABB**** gridObjects;
	glm::vec3 	axisLengths;
	glm::vec3 	gridOrigin;
	glm::vec3 	numCells;

	MxUniformGrid()
	{

	}

	~MxUniformGrid()
	{
		free(gridObjects);
	}

	
};


#endif
