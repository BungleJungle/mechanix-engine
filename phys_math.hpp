#pragma once

#include <iostream>
#include <math.h>

#ifndef PHYS_MATH
#define PHYS_MATH

#include "glm/glm.hpp"

static inline float vMag(glm::vec3 a)
{
	return sqrt(a.x * a.x + a.y * a.y + a.z * a.z);
}

#endif	