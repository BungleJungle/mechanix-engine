#include "entity.hpp"

FbInstance* createFabrykInstance(const char* filePath, uint32_t instanceFlags, uint32_t maxObjects)
{
	FbInstance* newInst = (FbInstance*)calloc(1, sizeof(FbInstance));
	if (newInst == NULL)
	{
		printf("Failed to create instance %i \n", fbInstanceCounter);
		return NULL;
	}

	newInst->gameObjects = (FbGameObject*)calloc(maxObjects, sizeof(FbGameObject));
	if (newInst->gameObjects == NULL)
	{
		printf("Fatal no mem \n");
		return NULL;
	}

	newInst->objectDeltas = (uint8_t*)calloc(maxObjects / 8 + 1, sizeof(uint8_t));
	if (newInst->objectDeltas == NULL)
	{
		//
		printf("Fatal no mem \n");
		return NULL;
	}

	if (instanceFlags & FB_FLAGS_DEBUG_MODE)
	{
		printf("Fb instance %i, created in DEBUG mode \n", fbInstanceCounter);
		for (int i = 0; i < maxObjects; ++i)
		{
			newInst->gameObjects[i].compType = (uint8_t*)calloc(8, sizeof(uint8_t));
			newInst->gameObjects[i].components = (void**)calloc(8, sizeof(void*));
			newInst->gameObjects[i].maxComponents = 8;
			newInst->gameObjects[i].numComponents = 0;
		}
	}

	newInst->instanceFlags = instanceFlags;

	return newInst;
}

int updateObjectModelMatricies(FbInstance* fbInst)
{
	// 
	for (int i = 0; i < fbInst->numLoadedGameObjects; ++i)
	{
		//
		updateModelMatrix(&fbInst->gameObjects[i]);
	}

	return 0;
}

uint32_t _debug_setObjectData(FbInstance* fb, glm::vec3 wPosition, uint64_t objId)
{
	FbGameObject* gPtr = (FbGameObject*)&(fb->gameObjects[objId & 0x00000000FFFFFFFF]);
	gPtr->worldPosition = wPosition;
	gPtr->objectId = objId;

	gPtr->scale = glm::vec3(1, 1, 1);
	gPtr->orientation = glm::quat(0, 0, 0, 1);

	printf("Object %i at postition = %f, %f, %f \n", objId,
		fb->gameObjects[objId].worldPosition.x,
		fb->gameObjects[objId].worldPosition.y,
		fb->gameObjects[objId].worldPosition.z);

	return 0;
}
