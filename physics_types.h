#include <iostream>
#include <list>

#ifndef PHYSICS_TYPES_H
#define PHYSICS_TYPES_H

#include "entity.hpp"
#include "collision_types.h"

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/quaternion.hpp>

/*
	COLLISION API
*/



struct GridMember
{
	GridMember* next;
	uint32_t 	id;
};

enum
{
	OBJECT_NO_MOVEMENT,
	OBJECT_WILL_MOVE
};

struct MxForce
{
	MxForce*	next;
	glm::vec3	linear;
	glm::vec3	angular;	
	float		duration;		// duration is in mS
};

struct MxRigidBody
{
	MxForce*		forces;
	FbGameObject*	masterObj;
	glm::vec3 		position;
	glm::vec3 		velocity;
	glm::vec3 		angularPosition;
	glm::vec3		angularVelocity;
	float 			frictionCoeff;
	float			elasticCoeff;
	float			invMass;
	uint32_t		physId;
	uint32_t 		colSlave;
	uint32_t		flags : 28;
	uint32_t		numForces : 4;

	//  uint32_t		flags : 28;
#define RIGID_NO_GRAVITY 		0x00000001
#define RIGID_NO_COLLISION		0x00000002
};

struct MechanixInstance
{
	MxRigidBody*			rigidBodies;
	Contacts*				contactQueue;
	uint8_t*				deltaBits;
	uint32_t				maxBodies;
	uint32_t				numLoadedBodies;
	uint32_t				instanceId;

	MxCollisionInstance*	colInst;
	uint32_t				instanceFlags;
	glm::vec3 				gravtiyVector;
};


/*
	PHYSICS API
*/



#endif