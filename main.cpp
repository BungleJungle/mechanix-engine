#include <iostream>

#include "collision.hpp"
#include "physics_types.h"
#include "physics.hpp"
#include "entity.hpp"
#include "chrono"
#include "thread"


int main()
{
	printf("starting \n");
	FbInstance* fbInst = createFabrykInstance(NULL, FB_FLAGS_DEBUG_MODE, 16);
	MechanixInstance* const mx = createMechanixInstance(NULL, MX_FLAGS_DEBUG_MODE | MX_RENDER_COLL_VERT, 16);

	if (loadMechanixObject(mx, "C:/Users/Steve/source/repos/mechanix/res/test_obj.mxo") == MX_FAILED_TO_LOAD)
	{
		printf("Failed to load \n");
	}
	if (loadMechanixObject(mx, "C:/Users/Steve/source/repos/mechanix/res/test_obj.mxo") == MX_FAILED_TO_LOAD)
	{
		printf("Failed to load \n");
	}
	if (loadMechanixObject(mx, "C:/Users/Steve/source/repos/mechanix/res/test_obj_1.mxo") == MX_FAILED_TO_LOAD)
	{
		printf("Failed to load \n");
	}
	if (loadMechanixObject(mx, "C:/Users/Steve/source/repos/mechanix/res/test_obj_2.mxo") == MX_FAILED_TO_LOAD)
	{
		printf("Failed to load \n");
	}
	if (loadMechanixObject(mx, "C:/Users/Steve/source/repos/mechanix/res/test_obj_3.mxo") == MX_FAILED_TO_LOAD)
	{
		printf("Failed to load \n");
	}
	if (loadMechanixObject(mx, "C:/Users/Steve/source/repos/mechanix/res/test_obj_4.mxo") == MX_FAILED_TO_LOAD)
	{
		printf("Failed to load \n");
	}

	// testing force removeal:
	MxForce* temp = (MxForce*)calloc(1, sizeof(MxForce));
	temp->linear = glm::vec3(0, 0, -20.0f);
	temp->duration = 0.2f;
	temp->angular = glm::vec3(0, 0, 0);
	addForceToObject(mx, 0, temp);

	initPhysics(mx, MX_INSTANCE_NO_FB_LINK);

	buildUniformGrid(mx->colInst, glm::vec3(10, 10, 10), glm::vec3(250, 250, 250), glm::vec3(0, 0, 0));
	populateGrid(mx->colInst);


	while (mx->instanceFlags & MX_INSTANCE_SHOULD_LIVE)
	{
		// do shit here
		updatePhysics(mx, 0.016);
		
		for (int i = 0; i < mx->numLoadedBodies; ++i)
		{
			printf("%i : position		= <%f, %f, %f> \n", i, mx->rigidBodies[i].position.x, 
				mx->rigidBodies[i].position.y, 
				mx->rigidBodies[i].position.z);

			printf("%i : Collision position  = <%f, %f, %f> \n", i, mx->colInst->pirimitives[i].centroid.x,
				mx->colInst->pirimitives[i].centroid.y,
				mx->colInst->pirimitives[i].centroid.z);
		}

		resolveContacts(mx);

		printf("\n");
		std::this_thread::sleep_for(std::chrono::milliseconds(500));
	}
	
	return 0;
}
